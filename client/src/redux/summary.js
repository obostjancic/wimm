import request from './../common/request';
import {getBalances} from "./dashboard";

export const GET_ENTRIES = 'GET_ENTRIES';
export const ADD_EMPTY_ENTRY = 'ADD_EMPTY_ENTRY';
export const ADD_ENTRY = 'ADD_ENTRY';
export const CLEAR_ENTRIES = 'CLEAR_ENTRIES';

const initialState = {
	entries: [],
	fetching: false,
	fetched: false
};

const summary = (state = initialState, action = {}) => {
	switch (action.type) {
		case `${GET_ENTRIES}_PENDING`:
			return {
				...state, fetching: true, fetched: false
			};
		case `${GET_ENTRIES}_FULFILLED`:
			return {
				...state, fetching: false, fetched: true, entries: action.payload.data
			};
		case `${GET_ENTRIES}_REJECTED`:
			return {
				...state, fetching: false, fetched: false, error: action.payload.error
			};
		default:
			return state;
	}
};

export const getEntries = () => (dispatch) => (dispatch({
	type: GET_ENTRIES,
	payload: request.get('/tracker/entry/')
}));

export const addEmptyEntry = () => (dispatch) => (dispatch({
	type: ADD_EMPTY_ENTRY
}));

export const addEntry = (entry) => (dispatch) => {
    dispatch({
        type: ADD_ENTRY,
		payload: request.post(
			`/tracker/entry/?balanceId=${entry.balance}`, entry
		)
    }).then(() => dispatch(getEntries()))
	  .then(() => dispatch(getBalances()))
};

export default summary;
