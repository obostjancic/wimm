import request from "../common/request";

export const TOGGLE_SIDEBAR = 'TOGGLE_SIDEBAR';
export const GET_STORES = 'GET_STORES';
export const GET_CATEGORIES = 'GET_CATEGORIES';
export const GET_STATS_TIME = 'GET_STATS_TIME';
export const GET_STATS_CATEGORY = 'GET_STATS_CATEGORY';
export const GET_BALANCES = 'GET_BALANCES';
export const GET_SCHEDULED_INCOME = 'GET_SCHEDULED_INCOME';

const initialState = {
    sidebarVisible: false,
    stores: [],
	categories: [],
	statsTime: [],
	statsCategory: [],
	balances: [],
	income: { amount: 0, category: { name: ''} },
	fetching: false,
	fetched: true
};

const dashboard = (state = initialState, action = {}) => {
    switch (action.type) {
        case TOGGLE_SIDEBAR:
            return {
                ...state, sidebarVisible: !state.sidebarVisible
            };
        case `${GET_STORES}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_STORES}_FULFILLED`:
			return {
				...state, geting: false, geted: true, stores: action.payload.data
			};
		case `${GET_STORES}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};
		case `${GET_CATEGORIES}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_CATEGORIES}_FULFILLED`:
			return {
				...state, geting: false, geted: true, categories: action.payload.data
			};
		case `${GET_CATEGORIES}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};
		case `${GET_STATS_TIME}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_STATS_TIME}_FULFILLED`:
			return {
				...state, geting: false, geted: true, statsTime: action.payload.data
			};
		case `${GET_STATS_TIME}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};	
		case `${GET_STATS_CATEGORY}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_STATS_CATEGORY}_FULFILLED`:
			return {
				...state, geting: false, geted: true, statsCategory: action.payload.data
			};
		case `${GET_STATS_CATEGORY}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};	
		case `${GET_BALANCES}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_BALANCES}_FULFILLED`:
			return {
				...state, geting: false, geted: true, balances: action.payload.data
			};
		case `${GET_BALANCES}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};	
		case `${GET_SCHEDULED_INCOME}_PENDING`:
			return {
				...state, geting: true, geted: false
			};
		case `${GET_SCHEDULED_INCOME}_FULFILLED`:
			return {
				...state, geting: false, geted: true, income: action.payload.data
			};
		case `${GET_SCHEDULED_INCOME}_REJECTED`:
			return {
				...state, geting: false, geted: false, error: action.payload.error
			};
        default:
            return state;
    }
};

export const toggleSidebar = () => (dispatch) => (dispatch({
    type: TOGGLE_SIDEBAR,

}));

export const getStores = () => (dispatch) => (dispatch({
	type: GET_STORES,
	payload: request.get('/tracker/store/')
}));

export const getCategories = () => (dispatch) => (dispatch({
	type: GET_CATEGORIES,
	payload: request.get('/tracker/category/')
}));

export const getStatsTime = () => (dispatch) => (dispatch({
	type: GET_STATS_TIME,
	payload: request.get('/tracker/stats/time/')
}));

export const getStatsCategory = () => (dispatch) => (dispatch({
	type: GET_STATS_CATEGORY,
	payload: request.get('/tracker/stats/category/')
}));

export const getBalances = () => (dispatch) => (dispatch({
	type: GET_BALANCES,
	payload: request.get('/tracker/balance/')
}));

export const getScheduledIncome = () => (dispatch) => (dispatch({
	type: GET_SCHEDULED_INCOME,
	payload: request.get('/tracker/income/')
}));

export default dashboard;
