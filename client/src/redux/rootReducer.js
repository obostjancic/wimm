import { combineReducers } from 'redux';
import auth from './auth';
import summary from './summary'
import dashboard from "./dashboard";

const rootReducer = combineReducers({
    auth, summary, dashboard
});

export default rootReducer;
