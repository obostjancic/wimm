export const sum = arr => arr.reduce((p, c) => p + c, 0);
export const average = arr => sum(arr) / arr.length;