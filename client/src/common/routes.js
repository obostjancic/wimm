export const HOME = '/';
export const LOGIN = '/login';
export const LOGOUT = '/logout';
export const REGISTER = '/register';
export const LOGGED_IN = '/user';
export const PROFILE = '/user/profile';
export const DASHBOARD = '/user/dashboard';