import React, { Component } from 'react';
import { Menu } from 'semantic-ui-react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as authActions from '../../redux/auth';
import * as ROUTE from '../../common/routes';
import './navbar.css';

class Navbar extends Component {
    componentDidMount() {
    }
    render() {
        if (!this.props.auth.isLoggedIn) return (<Redirect to={ROUTE.HOME} />);
        return (
            <Menu pointing secondary className="navbarNoMargin">
                <Menu.Menu position="right">
                    <Menu.Item name="logout" onClick={this.props.actions.auth.logout} />
                </Menu.Menu>
            </Menu>
        );
    }
}

function mapStateToProps(store) {
    return {
        auth: { ...store.auth },
    };
}

function mapDispatchToProps(dispatch) {
    return {
        actions: {
            auth: bindActionCreators(authActions, dispatch),
        }
    };
}

export default connect(mapStateToProps, mapDispatchToProps)(Navbar);
