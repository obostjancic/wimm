import React, { Component } from 'react';
import {Header, Segment} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import { Chart } from 'react-google-charts';
import * as dashboardActions from '../../../redux/dashboard';
import './stats.css';
import { BLUE } from "../../../common/colors";

const options = {
    axisTitlesPosition: 'none',
    pointSize: 4,
    interpolateNulls: true,
    vAxis:{
        gridlines:{
            count: -1
        }
    },
    chartArea:{
        left: '25%',
        top: 0,
        width: '70%',
        height: '90%'
    },
    legend: {
        position: 'none'
    },
    colors: [BLUE],
    backgroundColor: { fill:'transparent' }
};

class ByCategory extends Component {
    componentDidMount(){
        this.props.actions.dashboard.getStatsCategory();
    }
    render(){
        const statData = this.props.dashboard.statsCategory.map(e => Object.values(e))
                                        .map(e => [e[0], e[1] * -1]);
        const temp = [['Category', 'Amount'], ...statData]
        return(
        <Segment.Group raised className="statCard lower">
            <Segment className="compactPadding">
                <Header>Expenses by Category</Header>
            </Segment>
            <Segment>
                <Chart
                    chartType="BarChart"
                    height={'32vh'}
                    options={options}
                    data={temp}
                />
            </Segment>
        </Segment.Group>
        );
    }
}

function mapStateToProps(store) {
	return {
		dashboard: { ...store.dashboard },
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			dashboard: bindActionCreators(dashboardActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ByCategory);