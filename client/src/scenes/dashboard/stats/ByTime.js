import React, { Component } from 'react';
import { Header, Segment } from 'semantic-ui-react';
import moment from "moment";
import { Chart } from 'react-google-charts';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as dashboardActions from '../../../redux/dashboard';
import {average} from "../../../common/sum-avg";
import {BLUE, ORANGE} from "../../../common/colors";

const options = {
    axisTitlesPosition: 'none',
    pointSize: 4,
    interpolateNulls: true,
    vAxis:{
        gridlines:{
            count: -1
        }
    },
    chartArea:{
        left: 20,
        top: 10,
        width: '95%',
        height: '80%'
    },
    legend: {
        position: 'none'
    },
    colors: [BLUE, ORANGE],
    backgroundColor: { fill:'transparent' }
};

class ByTime extends Component {
    componentDidMount(){
        this.props.actions.dashboard.getStatsTime();
    }
    render(){
        const statData = this.props.dashboard.statsTime.map(e => Object.values(e))
                                        .map(e => [moment(e[0]).format('DD.MM'), e[1] * -1]);
        const avg = average(statData.map(e => e[1]));
        const temp2 = [['Time', 'Money', 'Average'], ...statData.map(e => [...e, avg]), ];
        return(
        <Segment.Group raised className="statCard">
            <Segment className="compactPadding">
                <Header>Expense timeline</Header>
            </Segment>
            <Segment>
                <Chart
                    height="32vh"
                    chartType="LineChart"
                    options={options}
                    data={temp2}
                />
            </Segment>
        </Segment.Group>
        );
    }
}

function mapStateToProps(store) {
	return {
		dashboard: { ...store.dashboard },
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			dashboard: bindActionCreators(dashboardActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ByTime);