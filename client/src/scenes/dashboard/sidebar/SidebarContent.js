import React, { Component } from 'react';
import { Form } from 'semantic-ui-react';
import moment from "moment";


class SidebarContent extends Component {
    constructor(props){
        super(props);
        this.state = {amount: 0, store: null, category: null, balance: null, note: 'note', created_at: moment()};
        this.onChange = this.onChange.bind(this);
        this.onSubmit = this.onSubmit.bind(this);
    }
    onChange(event, {name, value}){
        console.log(name, value);
        this.setState({ [name]: value })
    }
    onSubmit(event){
        this.props.addEntry(this.state);
    }
    render(){
        return(
                <Form>
					<Form.Input
                        label="Amount:"
                        name="amount"
                        value={this.state.amount}
                        onChange={this.onChange}
                    />
                    <Form.Select
                        label="Balance:"
                        name="balance"
                        value={this.state.balance}
                        onChange={this.onChange}
                        options={this.props.balances.map(e => ({text: e.type.name, value: e.id}))}
                    />
					<Form.Select
                        label="Category:"
                        name="category"
                        value={this.state.category}
                        onChange={this.onChange}
                        options={this.props.categories.map(e => ({text: e.name, value: e.id, description: e.description}))}
                    />
					<Form.Select
                        label="Store:"
                        name="store"
                        value={this.state.store}
                        onChange={this.onChange}
                        options={this.props.stores.map(e => ({text: e.name, value: e.id}))}
                    />
                    <Form.Input
                        label="Note:"
                        name="note"
                        value={this.state.note}
                        onChange={this.onChange}
                    />
                    <Form.Group>
					<Form.Button
                        positive
                        onClick={this.onSubmit}
                    >
                        Add
                    </Form.Button>
                    <Form.Button
                        negative
                        onClick={this.props.close}
                    >
                        Cancel
                    </Form.Button>
                    </Form.Group>
				</Form>
        );
    }
}

export default SidebarContent;