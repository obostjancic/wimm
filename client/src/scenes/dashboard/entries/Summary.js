import React, { Component } from 'react';
import { Segment, Header, List, Loader, Icon, Grid} from 'semantic-ui-react'
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as summaryActions from '../../../redux/summary';
import * as dashboardActions from '../../../redux/dashboard';
import Entry from './Entry';

class Summary extends Component {
	
	
	componentDidMount(){
		console.log(this.props);
		this.props.actions.summary.getEntries();
		this.props.actions.dashboard.getStores();
		this.props.actions.dashboard.getCategories();
	}
	render(){
		const { entries } = this.props.summary;
		return(
			<Segment.Group raised className="summary">
				{this.props.summary.fetching && <Loader active />}
				<Segment className="compactPadding">
					<Grid columns={2}>
						<Grid.Row>
							<Grid.Column verticalAlign="middle">
								<Header>
									Summary
								</Header>
							</Grid.Column>
							<Grid.Column>
								<Icon
									size="large"
									name="plus"
									className="iconButton"
									style={{float: 'right'}}
									onClick={() => this.props.actions.dashboard.toggleSidebar()}
								/>
							</Grid.Column>
						</Grid.Row>
					</Grid>
				</Segment>
				<Segment>
					<List divided>
						{entries.slice(0, 10).map(
							e => (<Entry key={e.id} entry={e} />))
						}
					</List>
				</Segment>
			</Segment.Group>
		);
	}
}

function mapStateToProps(store) {
	return {
		summary: { ...store.summary },
		dashboard: { ...store.dashboard }
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			summary: bindActionCreators(summaryActions, dispatch),
			dashboard: bindActionCreators(dashboardActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Summary);