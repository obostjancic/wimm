import React, { Component } from 'react';
import {Grid, List} from 'semantic-ui-react';
import moment from "moment";
import {GREEN, RED} from "../../../common/colors";
import './entry.css';

class Entry extends Component {
	constructor(props){
		super(props);
		this.state = { ...this.props.entry };
	}
	render(){
		return (
			<List.Item>
				<Grid columns={2}>
					<Grid.Row>
						<Grid.Column width={12}>
								<div className="categoryStore">
									{this.state.category.name}
									&nbsp;at&nbsp;
									{this.state.store.name}
								</div>
								<div className="date">
									{moment(this.state.created_at).format('DD.MM.YYYY')}
								</div>
								<div className="note">
									{this.state.note}
								</div>
						</Grid.Column>
						<Grid.Column width={4} className="amount" verticalAlign="middle">
								{(this.state.amount) > 0
									? <dt style={{color: GREEN}}>{this.state.amount} &euro;</dt>
									: <dt style={{color: RED}}>{this.state.amount} &euro;</dt>
								}
						</Grid.Column>
					</Grid.Row>
				</Grid>
			</List.Item>
		);
	}
}

export default Entry;