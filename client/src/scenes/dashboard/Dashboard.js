import React, { Component } from 'react';
import {Grid, Segment, Sidebar} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as dashboardActions from '../../redux/dashboard';
import * as summaryActions from '../../redux/summary';
import ShoppingList from './shopping-list/ShoppingList';
import ByTime from './stats/ByTime';
import Summary from './entries/Summary';
import ByCategory from './stats/ByCategory';
import SidebarContent from './sidebar/SidebarContent';
import Budget from "./budget/Budget";
import './dashboard.css';

class Dashboard extends Component {
    constructor(props){
        super(props);
        this.addEntry = this.addEntry.bind(this);
    }
    addEntry(entry){
        this.props.actions.summary.addEntry(entry);
        this.props.actions.dashboard.toggleSidebar();
    }
    render(){
        const { stores, categories, balances } = this.props.dashboard;
        return(
            <Sidebar.Pushable>
                <Sidebar as={Segment} width='very wide' animation='overlay' direction='left' visible={this.props.dashboard.sidebarVisible}>
                    <SidebarContent
                        categories={categories}
                        balances={balances}
                        stores={stores}
                        addEntry={this.addEntry}
                        close={this.props.actions.dashboard.toggleSidebar}
                    />
                </Sidebar>
                <Sidebar.Pusher className="dashboard">
                    <Grid stackable padded columns={3}>
                        <Grid.Row stretched>
                            <Grid.Column>
                                <Budget />
                                <ShoppingList />
                            </Grid.Column>
                            <Grid.Column>
                                <Summary />
                            </Grid.Column>
                            <Grid.Column>
                                <ByTime />
                                <ByCategory />
                            </Grid.Column>
                        </Grid.Row>
                    </Grid>
                </Sidebar.Pusher>
            </Sidebar.Pushable>
        );
    }
}

function mapStateToProps(store) {
	return {
		dashboard: { ...store.dashboard },
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			dashboard: bindActionCreators(dashboardActions, dispatch),
            summary: bindActionCreators(summaryActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);
