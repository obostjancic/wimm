import React from 'react';
import {Grid, List} from 'semantic-ui-react';
import {GREEN} from "../../../common/colors";
import '../entries/entry.css';

const Balance = ({name, amount, bold}) =>  (
	<List.Item>
		<Grid columns={2}>
			<Grid.Row>
				<Grid.Column width={10}>
					{(bold) ? <b>{name}</b> : name}
				</Grid.Column>
				<Grid.Column width={6} className="amount" verticalAlign="middle">
					{(bold) ? <b style={{color: GREEN}}>{amount} &euro;</b> : <dt style={{color: GREEN}}>{amount} &euro;</dt>}

				</Grid.Column>
			</Grid.Row>
		</Grid>
	</List.Item>
);

export default Balance;