import React, { Component } from 'react';
import {Divider, Header, List, Segment} from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as dashboardActions from '../../../redux/dashboard';
import Balance from "./Balance";
import {sum} from "../../../common/sum-avg";
import moment from "moment";

class Budget extends Component {
    componentDidMount(){
        this.props.actions.dashboard.getBalances();
        this.props.actions.dashboard.getScheduledIncome();
    }
    render(){
        const {balances, income} = this.props.dashboard;
        const total = sum(balances.map(e => parseFloat(e.amount)));
        const incomeDays = moment(income.date).diff(moment(), 'days') + 2;
        return(
        <Segment.Group raised>
            <Segment className="compactPadding">
                <Header>Budget</Header>
            </Segment>
            <Segment>
                <List divided>
                    <List.Header><b>Balance</b></List.Header>
                    {balances.map(e => (
                        <List.Item key={e.id}>
                            <Balance name={e.type.name} amount={e.amount} />
                        </List.Item>
                    ))}
                    <Divider fitted />
                    <Balance bold name={'TOTAL'} amount={total.toFixed(2)} />
                </List>
                <div className="centerText">
                    Next scheduled income
                    ({income.category.name})
                    is in {incomeDays} days <br />
                    Average daily budget: {(total/incomeDays).toFixed(2)} &euro;
                </div>
            </Segment>
        </Segment.Group>
        );
    }
}

function mapStateToProps(store) {
	return {
		dashboard: { ...store.dashboard },
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			dashboard: bindActionCreators(dashboardActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(Budget);