import React, { Component } from 'react';
import { Header, Segment } from 'semantic-ui-react';
import {bindActionCreators} from 'redux';
import { connect } from 'react-redux';
import * as dashboardActions from '../../../redux/dashboard';

class ShoppingList extends Component {

    render(){
        return(
        <Segment.Group raised className="lower">
            <Segment className="compactPadding">
                <Header>Shopping list</Header>
            </Segment>
            <Segment>
            </Segment>
        </Segment.Group>
        );
    }
}

function mapStateToProps(store) {
	return {
		dashboard: { ...store.dashboard },
	};
}

function mapDispatchToProps(dispatch) {
	return {
		actions: {
			dashboard: bindActionCreators(dashboardActions, dispatch),
		}
	};
}

export default connect(mapStateToProps, mapDispatchToProps)(ShoppingList);