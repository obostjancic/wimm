Django==2.1.7
dj-database-url==0.5.0
django-heroku==0.3.1
psycopg2==2.7.7
whitenoise==4.1.2
gunicorn==19.9.0
djangorestframework==3.9.1
django-cors-headers==2.2.0