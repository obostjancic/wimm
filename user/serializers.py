"""
Created by Ognjen on 4.11.2017. Contains serializers for user and groups object.
"""

from django.contrib.auth.models import User, Group
from rest_framework import serializers


class UserRegisterSerializer(serializers.ModelSerializer):
    """
    Serializer class used for creating new User from registration form.
    Overridden create method (by default new user is not active = has
    disabled access).
    """
    class Meta:
        model = User
        fields = ('username', 'password', 'email', 'first_name', 'last_name')

    def create(self, validated_data) -> None:
        """
        Overridden create method of ModelSerializer.

        Args:
            validated_data: Data from User objects.
        """
        user = User.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            is_active=True,
            first_name=validated_data['first_name'],
            last_name=validated_data['last_name'],
        )
        user.set_password(validated_data['password'])
        user.save()

        return user


class UserSimpleSerializer(serializers.ModelSerializer):
    """
    Serializer class used to represent user that created a resource
    (Store, Category ...)
    """
    class Meta:
        model = User
        fields = ('username', 'first_name', 'last_name')


class GroupSerializer(serializers.ModelSerializer):
    """
    Serializer class to represent group. Used for changing user group.
    """
    class Meta:
        model = Group
        fields = ('id', 'name')
