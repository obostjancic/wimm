from django.urls import path
from . import views

urlpatterns = [

    # localhost:8000/user/login/
    path('login/', views.LoginView.as_view(), name='login'),

    # localhost:8000/user/register/
    path('register/', views.RegisterView.as_view(), name='register'),

    # localhost:8000/user/logout/
    path('logout/', views.LogoutView.as_view(), name='logout'),

]
