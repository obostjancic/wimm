"""
Created by Ognjen on 4.11.2017. Contains methods for user login, logout and
register.
"""

from datetime import datetime

from django.contrib.auth import authenticate
from django.contrib.auth.models import User
from rest_framework.authtoken.models import Token
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK, HTTP_404_NOT_FOUND
from rest_framework.status import HTTP_201_CREATED, HTTP_400_BAD_REQUEST
from rest_framework.status import HTTP_401_UNAUTHORIZED, HTTP_409_CONFLICT
from rest_framework.views import APIView

from core.models import Balance
from .serializers import UserRegisterSerializer


class LoginView(APIView):
    """
    Class based view for user login.
    """
    def post(self, req: Request) -> Response:
        """
        Performs user login. If the user already is logged gets his current
        token. If not creates a new token. Sets last_login field in user
        table to current time.

        Args:
            req: Initial client request

        Returns: HTTP 401 if user credentials are not valid.
                 HTTP 200 and response data containing token, first_name and
                 user group otherwise.
        """
        try:
            User.objects.get(username=req.data.get("username"))

        except User.DoesNotExist:
            return Response({"error": "Login failed! User does not exist."},
                            status=HTTP_404_NOT_FOUND)

        user = authenticate(username=req.data.get("username"),
                            password=req.data.get("password"))

        if not user:
            return Response({"error": "Login failed! Unauthorized!"},
                            status=HTTP_401_UNAUTHORIZED)

        user.last_login = datetime.utcnow()
        user.save()

        token, created = Token.objects.get_or_create(user=user)

        if not created:
            token.created = datetime.utcnow()
            token.save()

        response_data = {"first_name": user.first_name, "token": token.key}

        return Response(response_data, status=HTTP_200_OK)


class RegisterView(APIView):
    """
    Class based view for user registration.
    """
    def post(self, req: Request) -> Response:
        """
        User registration. Creates new User object.

        Args:
            req: Initial client request

        Returns: HTTP status 201 if new User was successfully created. HTTP
                 status 400 if request format is wrong HTTP status 409 if User
                 with that username/e-email address already exists
        """
        serializer = UserRegisterSerializer(data=req.data)

        user = User.objects.filter(username=req.data.get("username")) | \
               User.objects.filter(email=req.data.get("email"))

        if user:

            return Response(
                {"error": "User with the same username/email already exists."},
                status=HTTP_409_CONFLICT)

        if serializer.is_valid():

            user = serializer.save()
            balance = Balance(amount=0, user=user)
            balance.save()
            return Response(data=serializer.data, status=HTTP_201_CREATED)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)


class LogoutView(APIView):
    """
    Class based view for user logout.
    """
    def post(self, req: Request) -> Response:
        """
        Performs user logout. Deletes users token.

        Args:
            req: Initial client request

        Returns: HTTP status 200.
        """
        req.user.auth_token.delete()

        return Response(status=HTTP_200_OK)
