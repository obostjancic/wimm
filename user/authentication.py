"""
Created by Ognjen on 8.11.2017. Contains custom written classes for token
base authentication with expiring token.
"""

import pytz

from datetime import datetime, timedelta

from django.conf import settings
from rest_framework.authentication import TokenAuthentication
from rest_framework.authtoken.models import Token
from rest_framework.exceptions import AuthenticationFailed

EXPIRE_DAYS = getattr(settings, 'REST_FRAMEWORK_TOKEN_EXPIRE_DAYS', 60)


class ExpiringTokenAuthentication(TokenAuthentication):
    """
    Class based authentication. Derives TokenAuthentication. Adds token
    expiration time.
    """
    def authenticate_credentials(self, key):
        """
        Checks whether user authentication token is valid

        Args:
            key: Value of the token itself

        Returns:
            User object that has the specified token, token itself.
        """
        try:
            token = Token.objects.get(key=key)

        except Token.DoesNotExist:
            raise AuthenticationFailed('Invalid token')

        if not token.user.is_active:
            raise AuthenticationFailed('User inactive')

        utc_now = datetime.now()

        if token.created < pytz.utc.localize(utc_now) - timedelta(
                days=EXPIRE_DAYS):
            token.delete()

            raise AuthenticationFailed('Token has expired')

        return token.user, token
