"""
Created by Ognjen on 10.5.2018. Contains models for core tables in database.
"""

from django.contrib.auth.models import User
from django.db import models


class Category(models.Model):
    """
    Model for Category table. Representation of one category.
    """

    class Meta:
        db_table = 'category'

    name = models.CharField(max_length=250)
    description = models.CharField(max_length=250)

    def __str__(self) -> str:
        return "Category: " + self.name


class Store(models.Model):
    """
    Model for Category table. Representation of one category.
    """

    class Meta:
        db_table = 'store'

    name = models.CharField(max_length=250)

    def __str__(self) -> str:
        return "Store: " + self.name


class TrackerEntry(models.Model):
    """
    Model for tracker_entry table. Representation of one entry. Overridden
    default id - replaced with bigint.
    """
    class Meta:
        db_table = 'tracker_entry'

    id = models.BigAutoField(unique=True, primary_key=True)
    note = models.CharField(max_length=250)
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    category = models.ForeignKey(Category, db_column='category',
                                 related_name='te_category',
                                 on_delete=models.PROTECT)
    store = models.ForeignKey(Store, db_column='store',
                              related_name='te_store', on_delete=models.PROTECT)
    user = models.ForeignKey(User, db_column='user', related_name='te_user',
                             on_delete=models.PROTECT)
    created_at = models.DateTimeField()

    def __str__(self) -> str:
        return "Tracker entry: " + self.user.username + ", " + str(self.amount)


class BalanceType(models.Model):
    """
    Model for BalanceType table. Representation of one type of balance.
    """

    class Meta:
        db_table = 'balance_type'

    name = models.CharField(max_length=50)
    description = models.CharField(max_length=250)

    def __str__(self) -> str:
        return "Balance type: " + self.name + " " + self.description


class Balance(models.Model):
    """
    Model for Balance table. Representation of one users balance.
    """

    class Meta:
        db_table = 'balance'

    amount = models.DecimalField(decimal_places=2, max_digits=10)
    type = models.ForeignKey(BalanceType, db_column='type',
                             on_delete=models.PROTECT)
    user = models.ForeignKey(User, db_column='user',
                             related_name='b_user',
                             on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "Balance: " + self.user.username + " " + self.type.name + " " + \
               str(self.amount)


class Priority(models.Model):
    """
    Model for Priority table. Used for shopping list item to define it's
    priority.
    """

    class Meta:
        db_table = 'priority'

    name = models.CharField(max_length=50)

    def __str__(self) -> str:
        return "Priority: " + self.name


class ShoppingListItem(models.Model):
    """
    Model for shopping_list_item table. Represents one item that should be
    bought by the user.
    """

    class Meta:
        db_table = 'shopping_list_item'

    name = models.CharField(max_length=50)
    budget = models.DecimalField(decimal_places=2, max_digits=10)
    category = models.ForeignKey(Category, db_column='category',
                                 related_name='sli_category',
                                 on_delete=models.PROTECT)
    user = models.ForeignKey(User, db_column='user',
                             related_name='sli_user',
                             on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "Shopping list item: " + self.name + " " + str(self.budget)


class ScheduledIncome(models.Model):
    """
    Model for scheduled_income.
    """

    class Meta:
        db_table = 'scheduled_income'

    date = models.DateTimeField()
    amount = models.DecimalField(decimal_places=2, max_digits=10)
    category = models.ForeignKey(Category, db_column='category',
                                 related_name='si_category',
                                 on_delete=models.PROTECT)
    user = models.ForeignKey(User, db_column='user',
                             related_name='si_user',
                             on_delete=models.PROTECT)

    def __str__(self) -> str:
        return "Scheduled income: " + str(self.date) + " " + str(self.amount)
