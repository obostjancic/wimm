import time
import pytz

from datetime import datetime
from decimal import Decimal

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_200_OK, \
    HTTP_201_CREATED, HTTP_400_BAD_REQUEST, HTTP_204_NO_CONTENT
from rest_framework.views import APIView

from core.models import TrackerEntry, Store, Category, Balance
from core.serializers import StoreSerializer, CategorySerializer, \
    TrackerEntrySerializer, TrackerEntryFullSerializer


class StoreCRUD(APIView):
    # TODO add other methods
    """
    Class based view. CRUD operations for table store.
    """

    def get(self, req: Request, store_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request
            store_id: Primary key in table store (optional)

        Returns: If primary key is specified: Store object by primary key.
                 Else: List of all Store objects.
        """
        if store_id is not None:
            stores = Store.objects.get(pk=store_id)
            serializer = StoreSerializer(stores)

        else:
            stores = Store.objects.all().order_by('name')
            serializer = StoreSerializer(stores, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)


class CategoryCRUD(APIView):
    # TODO add other methods
    """
    Class based view. CRUD operations for table category.
    """

    def get(self, req: Request, category_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request
            category_id: Primary key in table category (optional)

        Returns: If primary key is specified: Category object by primary key.
                 Else: List of all Category objects.
        """
        if category_id is not None:
            categories = Category.objects.get(pk=category_id)
            serializer = CategorySerializer(categories)

        else:
            categories = Category.objects.all().order_by('name')
            serializer = CategorySerializer(categories, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)


class TrackerEntryCRUD(APIView):
    """
    Class based view. CRUD operations for table tracker_entry.
    """

    def get(self, req: Request, entry_id: int = None) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request
            entry_id: Primary key in table tracker_entry (optional)

        Returns: If primary key is specified: TrackerEntry object by primary
                 key.
                 Else: List of all TrackerEntry objects.
        """
        if entry_id is not None:
            entries = TrackerEntry.objects.get(pk=entry_id)
            serializer = TrackerEntryFullSerializer(entries)

        else:
            start = pytz.utc.localize(datetime.fromtimestamp(
                int(req.query_params.get('start', time.time() - 7 * 86400))))
            end = pytz.utc.localize(datetime.fromtimestamp(
                int(req.query_params.get('end', time.time()))))
            entries = TrackerEntry.objects.exclude(amount=0).filter(
                user=req.user.id, created_at__range=(start, end)).order_by(
                '-created_at')
            serializer = TrackerEntryFullSerializer(entries, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)


    def post(self, req: Request) -> Response:
        """
        POST request.
        Args:
            req: Incoming client request

        Returns: Saved TrackerEntry object (with assigned id) as JSON
        """
        serializer = TrackerEntrySerializer(data=req.data)

        if serializer.is_valid():
            entry = serializer.save(user=req.user)
            balance_id = int(req.query_params.get('balanceId', 0))
            if balance_id:
                balance = Balance.objects.get(id=balance_id)

                if balance.user.id != req.user.id:
                    return Response(data='Invalid balance id',
                                    status=HTTP_400_BAD_REQUEST)

                balance.amount += Decimal(entry.amount)
                balance.save()
                return Response(data=serializer.data, status=HTTP_201_CREATED)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def put(self, req: Request, entry_id: int) -> Response:
        """
        PUT request with pk specified in url
        Args:
            req: Incoming client request
            corpus_id: Primary key in table tracker_entry (path variable)

        Returns: Updated TrackerEntry object as JSON
        """
        entry = TrackerEntry.objects.get(pk=entry_id)
        serializer = TrackerEntrySerializer(entry, data=req.data)

        if serializer.is_valid():
            serializer.save()
            return Response(data=serializer.data, status=HTTP_200_OK)

        return Response(data=serializer.errors, status=HTTP_400_BAD_REQUEST)

    def delete(self, req: Request, entry_id: int) -> Response:
        """
        DELETE request with pk specified in url
        Args:
            req: Incoming client request
            corpus_id: Primary key in table tracker_entry (path variable)

        Returns: HTTP status 204 if the object was deleted successfully.
                 HTTP status 404 otherwise.
        """
        entry = TrackerEntry.objects.get(pk=entry_id)
        try:
            entry.delete()
            return Response(status=HTTP_204_NO_CONTENT)
        except AttributeError:
            return Response(status=HTTP_404_NOT_FOUND)
