"""
    Created by Ognjen on 10.5.2018. Contains classes for serializing
    tracker.models objects.
"""
from rest_framework import serializers
from .models import TrackerEntry, Category, Store, Balance, BalanceType, \
    ScheduledIncome


class StoreSerializer(serializers.ModelSerializer):
    """
    Serializer for Store model. Contains all fields.
    """
    class Meta:
        model = Store
        fields = '__all__'


class CategorySerializer(serializers.ModelSerializer):
    """
    Serializer for Store model. Contains all fields.
    """

    class Meta:
        model = Category
        fields = '__all__'


class TypeSerializer(serializers.ModelSerializer):
    """
    Serializer for BalanceType model. Contains all fields.
    """
    class Meta:
        model = BalanceType
        fields = '__all__'


class BalanceSerializer(serializers.ModelSerializer):
    """
    Serializer for Balance model. Contains amount.
    """
    type = TypeSerializer()

    class Meta:
        model = Balance
        fields = '__all__'


class TrackerEntrySerializer(serializers.ModelSerializer):
    """
    Simple serializer of TrackerEntry model. Contains basic info fields.
    """

    class Meta:
        model = TrackerEntry
        exclude = ('user',)


class TrackerEntryFullSerializer(serializers.ModelSerializer):
    """
    Simple serializer of TrackerEntry model. Contains basic info fields.
    """
    store = StoreSerializer()
    category = CategorySerializer()

    class Meta:
        model = TrackerEntry
        exclude = ('user',)


class ScheduledIncomeSerializer(serializers.ModelSerializer):
    """
    Simple serializer of ScheduledIncome model. Contains basic info fields.
    """
    category = CategorySerializer()

    class Meta:
        model = ScheduledIncome
        exclude = ('user',)
