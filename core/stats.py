import time
import pytz

from datetime import datetime

from django.db.models import Sum
from django.db.models.functions import Trunc
from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from core.models import TrackerEntry


class ByTimeStats(APIView):
    """
    Class based view. Serves statistics about expenses over time.
    """

    def get(self, req: Request) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request
        Returns: List of Expenses grouped by time
        """
        start = pytz.utc.localize(datetime.fromtimestamp(
            int(req.query_params.get('start', time.time() - 7 * 86400))))
        end = pytz.utc.localize(datetime.fromtimestamp(
            int(req.query_params.get('end', time.time()))))
        interval_point = req.query_params.get('interval', 'day')

        entries = TrackerEntry.objects.annotate(interval_point=Trunc(
            'created_at', interval_point)).values(
            'interval_point').annotate(
            total=Sum('amount')).filter(
            user=req.user.id, amount__lte=0).order_by(
            'interval_point')

        return Response(entries, status=HTTP_200_OK)


class ByCategoryStats(APIView):
    """
    Class based view. Serves statistics about expenses by categories.
    """

    def get(self, req: Request) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request
        Returns: List of Expenses grouped by time
        """
        start = pytz.utc.localize(datetime.fromtimestamp(
            int(req.query_params.get('start', time.time() - 7 * 86400))))
        end = pytz.utc.localize(datetime.fromtimestamp(
            int(req.query_params.get('end', time.time()))))
        interval_point = req.query_params.get('interval', 'day')

        entries = TrackerEntry.objects.values('category__name').annotate(
            total=Sum('amount')).filter(
            user=req.user.id, amount__lte=0).order_by('total')

        return Response(entries, status=HTTP_200_OK)
