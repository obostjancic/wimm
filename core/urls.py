from django.urls import path

from core.budget import BalanceCRUD, NextIncome
from core.stats import ByTimeStats, ByCategoryStats
from core.entry import StoreCRUD, CategoryCRUD, TrackerEntryCRUD


urlpatterns = [
    path('store/', StoreCRUD.as_view(), name='store'),
    path('store/<int:store_id>/', StoreCRUD.as_view(), name='get_store_by_pk'),

    path('category/', CategoryCRUD.as_view(), name='store'),
    path('category/<int:category_id>/', CategoryCRUD.as_view(),
         name='get_category_by_pk'),

    path('entry/', TrackerEntryCRUD.as_view(), name='entry'),
    path('entry/<int:entry_id>/', TrackerEntryCRUD.as_view(),
         name='get_entry_by_pk'),

    path('balance/', BalanceCRUD.as_view(), name='balance'),
    path('income/', NextIncome.as_view(), name='next_income'),

    path('stats/time/', ByTimeStats.as_view(), name='by_time_stats'),
    path('stats/category/', ByCategoryStats.as_view(),
         name='by_category_stats'),

]
