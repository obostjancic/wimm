from django.contrib import admin

from . import models

admin.site.register(models.TrackerEntry)
admin.site.register(models.Category)
admin.site.register(models.Store)
admin.site.register(models.Balance)
admin.site.register(models.BalanceType)
admin.site.register(models.ShoppingListItem)
admin.site.register(models.Priority)
admin.site.register(models.ScheduledIncome)
