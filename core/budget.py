from datetime import datetime

from rest_framework.request import Request
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView

from core.models import Balance, ScheduledIncome
from core.serializers import BalanceSerializer, ScheduledIncomeSerializer


class BalanceCRUD(APIView):
    # TODO add other methods
    """
    Class based view. CRUD operations for table balance.
    """

    def get(self, req: Request) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request

        Returns: Current users balance
        """
        balance = Balance.objects.filter(user=req.user.id).order_by('amount')
        serializer = BalanceSerializer(balance, many=True)

        return Response(data=serializer.data, status=HTTP_200_OK)


class NextIncome(APIView):
    # TODO add other methods
    """
    Class based view.
    """

    def get(self, req: Request) -> Response:
        """
        GET request.
        Args:
            req: Incoming client request

        Returns: User's next scheduled income.
        """
        income = ScheduledIncome.objects.filter(
            user=req.user.id, date__gte=datetime.now()).order_by('date').first()
        # TODO handle no next income case
        serializer = ScheduledIncomeSerializer(income)
        return Response(data=serializer.data, status=HTTP_200_OK)
